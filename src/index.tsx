import React from 'react';
import ReactDom from 'react-dom';
import i18next from 'i18next';
import { i18nextReactInitConfig } from '@ijl/cli';

i18next.t = i18next.t.bind(i18next);

const i18nextPromise = i18nextReactInitConfig(i18next);

import App from './app';

export default () => <App/>;

const appElement = document.getElementById('app');

export const mount = async (Сomponent) => {
    await Promise.all([i18nextPromise]);
    ReactDom.render(
        <Сomponent/>,
        appElement
    );

    if((module as any).hot) {
        (module as any).hot.accept('./app', () => {
            ReactDom.render(
                <App />,
                appElement
            );
        })
    }
};

export const unmount = () => {
    ReactDom.unmountComponentAtNode(appElement);
};