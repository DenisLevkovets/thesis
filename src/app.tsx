import React, { StrictMode } from 'react';
import {
    BrowserRouter
} from "react-router-dom";
import { Provider } from 'react-redux';

import { store } from './__data__/store/index'

import {Dashboard} from './containers/dashboard';
import './app.css';

const App = () => (
        <BrowserRouter>
            <StrictMode>
                <Dashboard />
            </StrictMode>
        </BrowserRouter>
);

export default App;